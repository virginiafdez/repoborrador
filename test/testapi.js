const mocha = require('mocha');
const chai = require ('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);
var should = chai.should();

describe("First test",
  function () {
    it('Tests that duckduckgo works', function(done) {
      chai.request('http://www.duckduckgo.com')
        .get('/')
        .end(
          function (err, res) {
            console.log("Request finished");
            // console.log(res);
            // console.log(err);
            done();
            }
          )
        }
      )
    }
  )
  describe("Test de API de usuarios",
    function () {
      it('Tests that user api says hello', function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
            function (err, res) {
              console.log("Request finished");
              res.should.have.status(200);
              res.body.msg.should.be.eql("Hola desde API Techu");
              done();
              }
            )
          }
        ),
      it('Tests that user api returns user list', function(done) {
          chai.request('http://localhost:3000')
            .get('/apitech/v1/users')
            .end(
              function (err, res) {
                console.log("Request finished");
                res.should.have.status(200);
                res.body.users.should.be.a('array');
                for (user of res.body.users) {
                  user.should.have.property('id');
                  user.should.have.property('email');
                  user.should.have.property('password');
                }
                done();
                }
              )
            }
        )
      }
    )
    {
      "first_name" : "Server",
      "last_name" : "iano",
      "quesos" : ["Dehesa de los LLanos", "Montescusa"],
      "amigotes" : [
        {
          "name" : "Manolo",
          "de_quien_es" : "el de Pistolas"
        }, {
          "name" : "Indalecio",
          "de_quien_es" : " el de Pelala"
        }
      ],
      "queso_favorito" : {
        "name" : "Dehesa de los LLanos",
        "milk" : "cabra"
      }
    }
