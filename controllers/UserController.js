const io = require('../io');
const requestJson = require('request-json');
const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechuvfb12ed/collections/";
const mLabAPIkey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea";
const crypt = require('../crypt');

function getUsersV1(req, res) {
  console.log("GET /apitech/v1/users");

 var result = {};
 var users = require('../usuarios.json');

 if (req.query.$count == "true") {
   console.log("Count needed");
   result.count = users.length;
 }

   result.users = req.query.$top ?
   users.slice(0, req.query.$top) : users;

 res.send(result);
 }

module.exports.getUsersV1 = getUsersV1;

function getUsersV2(req,res){
  console.log("GET/apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");
  httpClient.get("user?" + mLabAPIkey,
    function(err, resMlab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }

  );
}
function getUserByIdV2(req,res) {
  console.log("GET/apitechu/v2/users/:id");

  var id = req.params.id;
  console.log("La id del usuario a obtener es " + id);
  var query = 'q={"id": '+ id +'}';
  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIkey,
  function(err, resMlab, body) {

    if (err) {
      var response = {
        "msg" : "Error obteniendo usuarios"
      }
      res.status(500);
    } else {
      if (body.length > 0) {
        var response = body[0] ;
      } else {
        var response = {
          "msg" : "Usuario no encontrado"
        }
        res.status(404);
      }
      }
          res.send(response);
     }
    )
}

module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;


function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");

  console.log(req.body);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);


 console.log(newUser)
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
    }
  var users = require('../usuarios.json');
  users.push(newUser);
  console.log("usuario añadido al array")
  io.writeUserDataToFile(users);
  res.send({"msg": "usuario creado"})
}
module.exports.createUserV1 = createUserV1;

function createUserV2(req, res){
  console.log("POST/apitechu/v2/users") ;

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
    }

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");
  httpClient.post("user?" + mLabAPIkey, newUser,
  function(err, resMlab, body) {
      console.log("Usuario creado");
      res.status(201).send({"msg": "Usuario creado"});
    }
  );
}
module.exports.createUserV2 = createUserV2;

function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("La id del usuario a borrar es" + req.params.id);

  var users = require('../usuarios.json');
  users.splice(req.params.id - 1, 1);

  io.writeUserDataToFile(users);

  res.send({"msg":"usuario borrado"});
  }

module.exports.deleteUserV1 = deleteUserV1;
/*

function deleteUserV1(req, res) {

   console.log("DELETE /apitechu/v1/users/:id");
   console.log("id es " + req.params.id);

   var users = require('../usuarios.json');

   var deleted = false;

   // console.log("Usando for normal");
   // for (var i = 0; i < users.length; i++) {
   //   console.log("comparando " + users[i].id + " y " +  req.params.id);
   //   if (users[i].id == req.params.id) {
   //     console.log("La posicion " + i + " coincide");
   //     users.splice(i, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for in");
   // for (arrayId in users) {
   //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
   //   if (users[arrayId].id == req.params.id) {
   //     console.log("La posicion " + arrayId " coincide");
   //     users.splice(arrayId, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for of");
   // for (user of users) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición ? coincide");
   //     // Which one to delete? order is not guaranteed...
   //     deleted = false;
   //     break;
   //   }
   // }

   // console.log("Usando for of 2");
   // // Destructuring, nodeJS v6+
   // for (var [index, user] of users.entries()) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posicion " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for of 3");
   // var index = 0;
   // for (user of users) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //     break;
   //   }
   //   index++;
   // }

   // console.log("Usando Array ForEach");
   // users.forEach(function (user, index) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posicion " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //   }
   // });

   // console.log("Usando Array findIndex");
   // var indexOfElement = users.findIndex(
   //   function(element){
   //     console.log("comparando " + element.id + " y " +   req.params.id);
   //     return element.id == req.params.id
   //   }
   // )
   //
   // console.log("indexOfElement es " + indexOfElement);
   // if (indexOfElement >= 0) {
   //   users.splice(indexOfElement, 1);
   //   deleted = true;
   // }

   //if (deleted) {
  //   io.writeUserDataToFile(users);
   //}

//   var msg = deleted ?
//     "Usuario borrado" : "Usuario no encontrado."

//   console.log(msg);
//   res.send({"msg" : msg});
 };
//}
);
*/
