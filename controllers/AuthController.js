const io = require('../io');

function getUsersV1(req, res) {
  console.log("GET /apitech/v1/users");

 var result = {};
 var users = require('../usuarios.json');

 if (req.query.$count == "true") {
   console.log("Count needed");
   result.count = users.length;
 }

   result.users = req.query.$top ?
   users.slice(0, req.query.$top) : users;

 res.send(result);
}
module.exports.getUsersV1 = getUsersV1;

function loginUserV1(req, res){
  console.log("POST/apitechu/v1/login");

  console.log(req.body);
  console.log(req.body.email);
  console.log(req.body.password);

  var user = {
      "email" : req.body.email,
      "password" : req.body.password
    }
  var encontrado = false

  var user = require('../usuarios.json');

  for (var i = 0; i < user.length; i++) {
  if (user[i].email == req.body.email && user[i].password == req.body.password)
       {
        user[i].logged = true
       io.writeUserDataToFile(user);
       res.send({"msg": "login correcto", "id" : user[i].id})
       encontrado = true
     break;}
   }
  if (encontrado == false){
    res.send({"msg": "login incorrecto"})
  }

 }

module.exports.loginUserV1 = loginUserV1;

function logoutUserV1(req, res){
  console.log("POST/apitechu/v1/logout/:id");
  var encontrado = false
  var user = require('../usuarios.json');
  console.log("req.params:", req.params.id);
  for (var i = 0; i < user.length; i++) {

  if (user[i].id == req.params.id && user[i].logged === true)
       {
       delete user[i].logged
       io.writeUserDataToFile(user);
       res.send({"msg": "logout correcto", "id" : user[i].id})
       encontrado = true
     break;}
   }
  if (encontrado == false){
    res.send({"msg": "logout incorrecto"})
  }

 }
 module.exports.logoutUserV1 = logoutUserV1;
