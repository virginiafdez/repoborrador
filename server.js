const express = require("express");
const app = express();
app.use(express.json());


const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');

const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto 3000 " + port);

app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg":"Hola desde API Techu"});
  }

)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("parametros");
    console.log(req.params);

    console.log("query string");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("body");
    console.log(req.body);

//    res.send("respuesta")

  }
)

app.post("/apitechu/v1/users", userController.createUserV1);
app.post("/apitechu/v2/users", userController.createUserV2);

app.get("/apitech/v1/users", userController.getUsersV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUserByIdV2);

app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);

app.post("/apitechu/v1/login", authController.loginUserV1);

app.post("/apitechu/v1/logout/:id", authController.logoutUserV1);
